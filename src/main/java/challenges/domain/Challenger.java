package challenges.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findChallengersByEmailEquals" })
public class Challenger {

    @NotNull
    @Column(unique = true)
    @Pattern(regexp = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$")
    private String email;

    @NotNull
    @Size(min = 6)
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Challenge> owned_challenges = new HashSet<Challenge>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Challenge> challenges = new HashSet<Challenge>();

    @OneToMany(cascade = CascadeType.ALL)
    private Set<GoalStatus> goals = new HashSet<GoalStatus>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<challenges.domain.Challenger> friends = new HashSet<challenges.domain.Challenger>();
}
