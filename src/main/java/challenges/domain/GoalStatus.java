package challenges.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findGoalStatusesByChallenge", "findGoalStatusesByChallenger", "findGoalStatusesByGoal" })
public class GoalStatus {

    @Enumerated
    private GoalStatuses status;

    @NotNull
    @ManyToOne
    private Goal goal;

    @OneToOne
    private MetricGoalStatus metric;

    @NotNull
    @ManyToOne
    private Challenger challenger;
}
