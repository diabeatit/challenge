// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package challenges.domain;

import challenges.domain.Challenger;
import challenges.domain.Goal;
import challenges.domain.GoalStatus;
import challenges.domain.GoalStatuses;
import challenges.domain.MetricGoalStatus;

privileged aspect GoalStatus_Roo_JavaBean {
    
    public GoalStatuses GoalStatus.getStatus() {
        return this.status;
    }
    
    public void GoalStatus.setStatus(GoalStatuses status) {
        this.status = status;
    }
    
    public Goal GoalStatus.getGoal() {
        return this.goal;
    }
    
    public void GoalStatus.setGoal(Goal goal) {
        this.goal = goal;
    }
    
    public MetricGoalStatus GoalStatus.getMetric() {
        return this.metric;
    }
    
    public void GoalStatus.setMetric(MetricGoalStatus metric) {
        this.metric = metric;
    }
    
    public Challenger GoalStatus.getChallenger() {
        return this.challenger;
    }
    
    public void GoalStatus.setChallenger(Challenger challenger) {
        this.challenger = challenger;
    }
    
}
