package challenges.domain;


public enum GoalStatuses {

    not_completed, partially_completed, completed;
}
