package challenges.domain;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class MetricGoal {

    @NotNull
    private Float limit;

    @NotNull
    @Size(min = 2)
    private String description;

    @NotNull
    @OneToOne
    private Goal goal;
}
