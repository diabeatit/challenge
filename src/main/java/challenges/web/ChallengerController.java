package challenges.web;

import challenges.domain.Challenger;
import org.springframework.roo.addon.web.mvc.controller.finder.RooWebFinder;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/challengers")
@Controller
@RooWebScaffold(path = "challengers", formBackingObject = Challenger.class)
@RooWebFinder
public class ChallengerController {
}
