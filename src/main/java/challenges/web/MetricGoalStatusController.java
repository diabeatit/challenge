package challenges.web;

import challenges.domain.MetricGoalStatus;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/metricgoalstatuses")
@Controller
@RooWebScaffold(path = "metricgoalstatuses", formBackingObject = MetricGoalStatus.class)
public class MetricGoalStatusController {
}
